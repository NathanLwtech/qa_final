using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualBasic;
using System.Diagnostics;

namespace Final
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Process p1 = new Process();
            p1.StartInfo.FileName = "C:\\VS_Projects\\CalculateRate.exe";
            p1.StartInfo.Arguments = "100000 10 5";
            p1.StartInfo.UseShellExecute = false;
            p1.StartInfo.RedirectStandardOutput = true;

            p1.Start();
            p1.WaitForExit();

            string result = p1.StandardOutput.ReadToEnd();
            result = result.Substring(0, 8);
            Assert.AreEqual(result, "1,060.66");
        }
    }
}