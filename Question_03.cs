using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualBasic;

namespace Final
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            double pmt = Financial.Pmt(5.0 / 100 / 12, 120, -100000);
            pmt = Math.Round(pmt, 2);
            Assert.AreEqual(pmt, 1060.66);
        }
    }
}