using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace PMTCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            int amount;
            double years;
            double rate;

            Console.WriteLine("Mortgage Calculator");

            Console.Write("Mortgage Amount: ");
            amount = Convert.ToInt32(Console.ReadLine());

            Console.Write("Term(Years): ");
            years = Convert.ToInt32(Console.ReadLine());

            Console.Write("Rate(%): ");
            rate = Convert.ToInt32(Console.ReadLine());

            double pmt = Financial.Pmt(rate / 100 / 12, years * 12, -amount);
            pmt = Math.Round(pmt, 2);
            Console.WriteLine(pmt);
        }
    }
}