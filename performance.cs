using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.VisualBasic;

namespace Perfomance
{
    class Program
    {
        static double q3_function()
        {
            double pmt = Financial.Pmt(5.0 / 100 / 12, 120, -100000);
            pmt = Math.Round(pmt, 2);
            return pmt;
        }

        static string q4_function()
        {
            Process p1 = new Process();
            p1.StartInfo.FileName = "C:\\VS_Projects\\CalculateRate.exe";
            p1.StartInfo.Arguments = "100000 10 5";
            p1.StartInfo.UseShellExecute = false;
            p1.StartInfo.RedirectStandardOutput = true;

            p1.Start();
            p1.WaitForExit();

            string result = p1.StandardOutput.ReadToEnd();
            return result;
        }

        static void Main(string[] args)
        {
            /* Question 3 - Library Function - Performance Test */
            Console.WriteLine("Question 3 - Library Function");
            long q3_total = 0;
            Stopwatch stopwatch1 = new Stopwatch();
            for (int i = 0; i < 5; i++)
            {
                stopwatch1.Start();
                q3_function();
                stopwatch1.Stop();
                Console.WriteLine("Run {0} - {1} ms", i + 1, stopwatch1.ElapsedMilliseconds);
                q3_total += stopwatch1.ElapsedMilliseconds;
                stopwatch1.Restart();
            }

            Console.WriteLine("Average - {0} ms \n", q3_total / 5);

            /* Question 4 - Process Function - Performance Test */
            Console.WriteLine("Question 4 - Process Function");
            long q4_total = 0;
            Stopwatch stopwatch2 = new Stopwatch();
            for(int i = 0; i < 5; i++)
            {
                stopwatch2.Start();
                q4_function();
                stopwatch2.Stop();
                Console.WriteLine("Run {0} - {1} ms", i + 1, stopwatch2.ElapsedMilliseconds);
                q4_total += stopwatch2.ElapsedMilliseconds;
                stopwatch2.Restart();
            }

            Console.WriteLine("Average - {0} ms", q4_total / 5);
        }
    }
}